![Build Status](https://drone-ext.mcos.nc/api/badges/scrippy/scrippy-snmp/status.svg) ![License](https://img.shields.io/static/v1?label=license&color=orange&message=MIT) ![Language](https://img.shields.io/static/v1?label=language&color=informational&message=Python)


![Scrippy, mon ami le scrangourou](./scrippy-snmp.png "Scrippy, mon ami le scrangourou")

# `scrippy_snmp`

Fonctionnalités de base SNMP pour le cadriciel [`Scrippy`](https://codeberg.org/scrippy).

## Prérequis

### Modules Python

#### Liste des modules requis

Les modules listés ci-dessous seront installés automatiquement.

- pysnmp
- pysnmp-mibs

## Installation

### Manuelle

```bash
git clone https://codeberg.org/scrippy/scrippy-snmp.git
cd scrippy-snmp
python -m pip install -r requirements.txt
make install
```

### Avec `pip`

```bash
pip install scrippy-snmp
```

### Utilisation

Ce module permet des opérations *SNMP*  de base telles que `get`, `walk` and `set`.

Il prend en charge les versions *SNMP* 1 et 2. La version 3 est encore en développement.

#### Operations *SNMP*

##### Configuration

Comme dans l'implémentation *SNMP* en *shell*, il est d'abord nécessaire de configurer la connexion en fournissant généralement le nom d'hôte ou l'adresse IP distant, le port UDP sur lequel se connecter à l'hôte et la *communauté* qui est une identificateur secret autorisé à effectuer certaines opérations sur l'hôte distant.

```python
from scrippy_snmp.snmp import Snmp

HOST = "127.0.0.1"
PORT = 161
RO_COMMUNITY = "read-only-community"
RW_COMMUNITY = "read-write-community"
VERSION = "2c"

snmp = Snmp(host=HOST, port=PORT, community=RO_COMMUNITY, version=VERSION)
```

##### Get

L'opération get permet de récupérer une valeur d'un *OID* spécifique:

```python
LOCATION_OID = ".1.3.6.1.2.1.1.6.0"

snmp = Snmp(host=HOST, port=PORT, community=RO_COMMUNITY, version=VERSION)
location = snmp.get(LOCATION_OID)

print(location)
("SNMPv2-MIB::sysLocation.0", "Room 42")
```

La valeur de retour est un tuple de 2 éléments contenant:

-  L'*OID* au format texte
- La valeur de l'*OID*


##### Walk

L'opération `walk` permet de récupérer une collection de valeurs d'*OID* directement placés sous l'*OID* spécifié.

```python
SYSTEM_OID = ".1.3.6.1.2.1.1"

snmp = Snmp(host=HOST, port=PORT, community=RO_COMMUNITY, version=VERSION)
result = snmp.walk(SYSTEM_OID)

for value in result:
  print(value)

('SNMPv2-MIB::sysDescr.0', 'arcadia.local.mcos.nc 1148685499 FreeBSD 13.2-RELEASE')
('SNMPv2-MIB::sysObjectID.0', 'SNMPv2-SMI::enterprises.12325.1.1.2.1.1')
('SNMPv2-MIB::sysUpTime.0', '320223')
('SNMPv2-MIB::sysContact.0', 'sysmeister@example.com')
('SNMPv2-MIB::sysName.0', 'scrippy.example.com')
('SNMPv2-MIB::sysLocation.0', 'Room 42')
...
```

La valeur de retour est une liste de tuples de 2 éléments contenant:

-  L'*OID* au format texte
- La valeur de l'*OID*



##### Set

L'opération `set` permet de définir la valeur d'un *OID* spécifique.

```python
LOCATION_OID = ".1.3.6.1.2.1.1.6.0"

snmp = Snmp(host=HOST, port=PORT, community=RW_COMMUNITY, version=VERSION)
snmp.set(oid=LOCATION_OID, value="Room 200", datatype="str")
location = snmp.get(LOCATION_OID)

print(location)
("SNMPv2-MIB::sysLocation.0", "Room 200")
```

**Note:**
- L'objet `Snmp` doit avoir été configuré avec une communauté **ayant des permissions d'écriture sur l'hôte distant**.
- Le type de donnée doit être spécifié à l'aide du paramètres `datatype` (voir ci-dessous pour les `datatypes` disponibles).
- La méthode`Snmp.set()` ne retourne aucune valeur

##### Type de donnée disponibles

| Datatype | Équivalent SNMP | Information |
| --- | ---- | ------------- |
| null | Null | Valeur nulle |
| bits | Bits | La construction BITS représente une énumération de bits nommés. Cette collection est assignée à des valeurs non négatives et continues, en commençant par zéro. |
| int | Integer32 | Le type Integer32 représente des informations à valeurs entières entre -2^31 et 2^31-1 inclusivement. |
| str | OctetString | Le type OCTET STRING représente des données binaires ou textuelles arbitraires. La limite de taille est variable. 255 octets permettent une interopérabilité maximale. |
| ipaddr | IpAddress | Le type IpAddress représente une adresse Internet de 32 bits. Il est représenté sous forme d'OCTET STRING de longueur 4, en ordre de bytes de réseau. |
| counter | Counter32 | Le type Counter32 représente un entier sans signe qui augmente de manière monotone jusqu'à ce qu'il atteigne une valeur maximale de 2^32. |
| counter64 | Counter64 | Le type Counter64 représente un entier sans signe qui augmente de manière monotone jusqu'à ce qu'il atteigne une valeur maximale de 2^64-1, où il reboucle et recommence à augmenter à partir de zéro. |
| gauge | Gauge32 | Le type Gauge32 représente un entier sans signe, qui peut augmenter ou diminuer, mais ne doit jamais dépasser une valeur maximale, ni tomber en dessous d'une valeur minimale. La valeur maximale ne peut pas être supérieure à 2^32-1. |
| unsigned | Unsigned32 | Le type Unsigned32 représente des informations à valeurs entières entre 0 et 2^32-1 inclusivement (0 à 4294967295 en décimal). |
| ticks | TimeTicks | Le type TimeTicks représente un entier non négatif qui représente le temps, modulo 2^32 (4294967296 en décimal), en centièmes de seconde entre deux époques. |
