FROM debian:bullseye-slim
RUN apt-get update && \
    apt-get install -y snmp snmpd
COPY ./snmpd/snmpd.conf /etc/snmp/snmpd.conf
RUN chown -R root: /etc/snmp/snmpd.conf
ENTRYPOINT ["snmpd", "-c", "/etc/snmp/snmpd.conf", "-f"]
EXPOSE 161/udp
